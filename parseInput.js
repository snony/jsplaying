/*
Just parse the input and then
*/
function parseInput()
{
  // get element by id
  var values =  document.getElementById('textArea').value.split('\n');
  var data = {};
  var ttl_values = values.length;
  var elements = [];
  for(var i= 0; i<ttl_values; i++)
  {
    var conections = values[i].split(' ');
    elements.push({data: {id:i.toString()}});
    var ttl_conections = conections.length;
    for(var j=0; j<ttl_values;j++)
    {
      if(i!=j && (parseInt(conections[j])) > 0)
      {
        elements.push({data: {id: i.toString()+j.toString(), source: i.toString(), target: j.toString(), dist:conections[j]}});
      }

    }
  }

  return elements;
}
